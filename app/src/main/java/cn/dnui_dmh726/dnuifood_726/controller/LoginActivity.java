package cn.dnui_dmh726.dnuifood_726.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import cn.dnui_dmh726.dnuifood_726.R;
import cn.dnui_dmh726.dnuifood_726.bean.UserBean;
import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;
import cn.dnui_dmh726.dnuifood_726.model.UserModel;

public class LoginActivity extends AppCompatActivity
        implements onRetrofitListener<UserBean> {
    private UserModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        model = new UserModel();
        model.login("","",this);

    }

    @Override
    public void onSuccess(UserBean object) {

    }

    @Override
    public void onFalure(String msg) {

    }
}
