package cn.dnui_dmh726.dnuifood_726.bean;

/**
 * Created by X on 2018/10/22.
 */

public class ResultBean {
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}