package cn.dnui_dmh726.dnuifood_726.bean;

/**
 * Created by X on 2018/11/19.
 */

public class CollectStatus {

    private String user_id;
    private String shop_id;
    private  int flag;
    private String collected;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_food_id) {
        this.shop_id = shop_food_id;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getCollected() {
        return collected;
    }

    public void setCollected(String collected) {
        this.collected = collected;
    }
}
