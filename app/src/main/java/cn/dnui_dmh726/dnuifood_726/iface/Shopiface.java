package cn.dnui_dmh726.dnuifood_726.iface;

import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;

/**
 * Created by X on 2018/10/29.
 */

public interface Shopiface {
    void getAllShops(onRetrofitListener listener);

    void getFoodByShop(String shop_id, onRetrofitListener listener);

    void isCollected(String user_id,
                     String shop_food_id,
                     String flag,
                     onRetrofitListener listener);

    void userCollectShop(String user_id, String shop_id, onRetrofitListener listener);
}
