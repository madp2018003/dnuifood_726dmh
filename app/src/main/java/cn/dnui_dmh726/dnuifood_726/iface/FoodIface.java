package cn.dnui_dmh726.dnuifood_726.iface;


import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;

/**
 * Created by X on 2018/11/19.
 */

public interface FoodIface {


    void isCollected(String user_id,
                     String shop_food_id,
                     String flag,
                     onRetrofitListener listener);

    void userCollectFood(String user_id, String food_id, onRetrofitListener listener);

    void getFoodById(String food_id, onRetrofitListener listener);

    void getAllCommentsByFood(String food_id, onRetrofitListener listener);

    void getFoodBySearch(
            String search, onRetrofitListener listener);
    void getAllOrdersByUser(

            String user_id,
            onRetrofitListener listener);

    void getAllCommentsByUser(

            String user_id,
            onRetrofitListener listener);

    void insertComment(

            String item_id,
            String content,
            onRetrofitListener listener

    );
    void updateComment(

            String item_id,
            String content,
            onRetrofitListener listener

    );
    void deleteComment(

            String item_id,
            onRetrofitListener listener
    );
}