package cn.dnui_dmh726.dnuifood_726.iface;

import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;

/**
 * Created by X on 2018/10/15.
 */
public interface Useriface {

    void login(String username, String userpass,
               onRetrofitListener listener);

    void  register(String username,
                   String userpass,
                   String mobilenum,
                   String address,
                   String comment,
                   onRetrofitListener listener);
    void getAllUserCollection(
            String user_id,
            String flag,
            onRetrofitListener listener
    );
    void userCollectFood(
            String user_id,
            String food_id,
            onRetrofitListener listener);

    void userCollectShop(
            String user_id,
            String shop_id,
            onRetrofitListener listener);


    void getUserById(
            String user_id,
            onRetrofitListener listener
    );
    void updateUserById(

            String user_id,
            String username,
            String userpass,
            String mobilenum,
            String address,
            onRetrofitListener listener

    );
}
