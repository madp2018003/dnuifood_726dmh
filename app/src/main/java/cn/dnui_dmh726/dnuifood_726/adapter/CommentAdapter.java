package cn.dnui_dmh726.dnuifood_726.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cn.dnui_dmh726.dnuifood_726.bean.CommentInfo;
import cn.dnui_dmh726.dnuifood_726.bean.ResultBean;
import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;
import cn.dnui_dmh726.dnuifood_726.model.FoodModel;

public class CommentAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements onRetrofitListener<ResultBean> {
    private int pos;
    private FoodModel foodModel;
    private List<CommentInfo> list;
    private Context context;
    private LayoutInflater layoutInflater;

    public CommentAdapter(Context context, List list, int layoutResource) {
        this.context=context;
        this.list=list;
        layoutInflater = LayoutInflater.from(context);
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.from(viewGroup.getContext()).inflate(R.layout.shop_item6, viewGroup, false);
        return new CommentAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final CommentInfo entity = (CommentInfo) list.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.mycomment_orderid.setText(entity.getOrde_id());
        viewHolder.mycomment_foodname.setText(entity.getFoodname());
        viewHolder.mycomment_commenttime.setText(entity.getComment_time());
        viewHolder.mycomment_content.setText(entity.getContent());
        viewHolder.mycomment_shopname.setText("【" + entity.getShopname() + "】");

        viewHolder.mycomment_alter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText et = new EditText(context);
                et.setText(entity.getContent());
                new AlertDialog.Builder(context).setTitle("修改评论")
                        .setView(et)
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alterComment(entity.getItem_id(), et.getText().toString());
                            }
                        }).setNegativeButton("取消", null).show();

            }
        });
        viewHolder.mycomment_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                deleteComment(entity.getItem_id());
            }
        });

    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mycomment_foodname, mycomment_commenttime, mycomment_content,
                mycomment_shopname,mycomment_orderid;
        Button mycomment_alter, mycomment_delete;
        public ViewHolder(View itemView) {
            super(itemView);
            mycomment_orderid = (TextView) itemView.findViewById(R.id.mycomment_orderid);
            mycomment_foodname = (TextView) itemView.findViewById(R.id.mycomment_foodname);
            mycomment_commenttime = (TextView)
                    itemView.findViewById(R.id.mycomment_commenttime);
            mycomment_content = (TextView) itemView.findViewById(R.id.mycomment_content);
            mycomment_shopname = (TextView) itemView.findViewById(R.id.mycomment_shopname);
            mycomment_alter = (Button) itemView.findViewById(R.id.mycomment_alter);
            mycomment_delete = (Button) itemView.findViewById(R.id.mycomment_delete);
        } }

    private void alterComment(String item_id,String et){
        foodModel=new FoodModel();
        foodModel.updateComment(item_id,et,this);
    }
    private void deleteComment(String item_id){
        foodModel=new FoodModel();
        foodModel.deleteComment(item_id,this);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onSuccess(ResultBean status, int flag) {
        if(flag==9) {
            if(status.getSuccess().equals("1")){
                Toast.makeText(context,"修改成功", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(context,"修改失败", Toast.LENGTH_SHORT).show();
            }
        }else
        {
            if(status.getSuccess().equals("1")){
                removeData(pos);
                Toast.makeText(context,"删除成功", Toast.LENGTH_SHORT).show(); }
            else{
                Toast.makeText(context,"删除失败", Toast.LENGTH_SHORT).show(); }
        }
    }

    public void removeData(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }
    @Override
    public void onFailure(String msg) {

    }
}