package cn.dnui_dmh726.dnuifood_726.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import cn.dnui_dmh726.dnuifood_726.R;
import cn.dnui_dmh726.dnuifood_726.bean.GetCollected;
import cn.dnui_dmh726.dnuifood_726.bean.ResultBean;
import cn.dnui_dmh726.dnuifood_726.controller.FoodInfoActivity;
import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;
import cn.dnui_dmh726.dnuifood_726.model.Constants;
import cn.dnui_dmh726.dnuifood_726.model.UserModel;

public class CollectAdapter extends RecyclerView.Adapter<CollectAdapter.ViewHolder>
        implements onRetrofitListener<ResultBean> {
    private UserModel userModel;
    private String shop_id, food_id;
    private Context context;
    private String user_id;
    private List<GetCollected> list;
    private LayoutInflater layoutInflater;
    private ArrayList data;

    public CollectAdapter(Context context, List list, int layoutResource) {

        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.from(viewGroup.getContext()).inflate(R.layout.shop_item4, viewGroup, false);
        return new CollectAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CollectAdapter.ViewHolder holder, int position) {

        final GetCollected entity = (GetCollected) list.get(position);
        if (entity.getFlag().equals("0")) {
            holder.collect_name.setText(entity.getShopname());
            holder.collect_content.setText(entity.getAddress());
            if (!entity.getPic().equals("")) {
                Picasso.with(context).load(Constants.BASE_URL + entity.getPic()).into(holder.collect_pic);
            }
            holder.collect_enter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context,FoodListActivity.class);
                    intent.putExtra("shop_id",entity.getShop_id().toString()+"");
                    intent.putExtra("shop_name",entity.getShopname().toString()+"");


                    context.startActivity(intent);
                    //跳转至店铺详细页
                }
            });
            holder.collect_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uncollectShop(entity.getShop_id());
                }
            });
        } else if(entity.getFlag().equals("1"))
        {
            holder.collect_name.setText(entity.getFoodname());
            holder.collect_content.setText(entity.getPrice() + "元");
            if (!entity.getPic().equals("")) {
                Picasso.with(context).load(Constants.BASE_URL + entity.getPic()).into(holder.collect_pic);
            }
            holder.collect_enter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) { //跳转至菜谱// 页
                    Intent intent = new Intent(context, FoodInfoActivity.class);
                    intent.putExtra("food_id",entity.getFood_id().toString()+"");

                    intent.putExtra("foodname",entity.getFoodname().toString()+"");

                    context.startActivity(intent);
                }
            });
            holder.collect_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uncollectFood(entity.getFood_id());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }




    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView collect_pic;
        private TextView collect_name, collect_content;
        private Button collect_enter, collect_cancle;

        public ViewHolder(View itemView) {
            super(itemView);
            collect_pic = (ImageView) itemView.findViewById(R.id.res_image);
            collect_name = (TextView) itemView.findViewById(R.id.res_name);
            collect_content = (TextView) itemView.findViewById(R.id.res_address);

            collect_enter = (Button) itemView.findViewById(R.id.button8);
            collect_cancle = (Button) itemView.findViewById(R.id.button7);
        }
    }

    private void uncollectShop(String Shop_id) {

        this.shop_id = Shop_id;
        userModel= new UserModel();


        userModel.userCollectShop(user_id, Shop_id, this);

        //取消收藏店铺
    }




    private void uncollectFood(String Food_id) {
        this.food_id = Food_id;
        userModel= new UserModel();

        userModel.userCollectFood(user_id,Food_id, this);

        //取消收藏菜谱
    }

    @Override
    public void onSuccess(ResultBean status, int flag) {
        if (flag == 3) {
            if (status.getSuccess().equals("1")) {
                Toast.makeText(context, "取消收藏菜谱成功", Toast.LENGTH_SHORT).show();

                for (int i = 0; i < data.size(); i++) {
                    GetCollected object = (GetCollected) data.get(i);
                    if (object.getFood_id().equals(food_id)) {
                        data.remove(object);
                        notifyDataSetChanged();
                        break;
                    }
                }
            } else {
                Toast.makeText(context, "取消收藏菜谱失败", Toast.LENGTH_SHORT).show();
            }

        }
//处理取消收藏菜谱

        else

        //处理取消收藏店铺
        {
            if (status.getSuccess().equals("1")) {
                Toast.makeText(context, "取消收藏店铺成功", Toast.LENGTH_SHORT).show();

                for (int i = 0; i < data.size(); i++) {
                    GetCollected object = (GetCollected) data.get(i);
                    if (object.getShop_id().equals(shop_id)) {
                        data.remove(object);
                        notifyDataSetChanged();
                        break;
                    }
                }
            } else {
                Toast.makeText(context, "取消收藏店铺失败", Toast.LENGTH_SHORT).show();
            }

        }

    }

    @Override
    public void onFailure(String msg) {

    }



}