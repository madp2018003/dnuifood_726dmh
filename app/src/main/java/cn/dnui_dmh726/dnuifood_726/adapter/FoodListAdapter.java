package cn.dnui_dmh726.dnuifood_726.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import cn.dnui_dmh726.dnuifood_726.bean.FoodBean;
import cn.dnui_dmh726.dnuifood_726.controller.FoodInfoActivity;
import cn.dnui_dmh726.dnuifood_726.model.Constants;

public class  FoodListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private  String phonenum;
    private Context context;
    private List<FoodBean> list;
    private LayoutInflater layoutInflater;

    public FoodListAdapter(Context context, List list, int layoutResource, String phonenum) {
        this.context=context;
        this.list=list;
        layoutInflater = LayoutInflater.from(context);
        this.phonenum=phonenum;
    }


    public void setList(List<FoodBean> list) {
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = layoutInflater.from(viewGroup.getContext()).inflate(R.layout.shop_item2, viewGroup, false);
        return new FoodListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final FoodBean food = (FoodBean) list.get(position);
        if (food==null)
            return;
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.food_foodname.setText(food.getFoodname());
        viewHolder.food_price.setText(food.getPrice()+"元");
        viewHolder.food_intro.setText(food.getIntro());

        if(!food.getPic().equals("")){
            Picasso.with(context).load(         Constants.BASE_URL+food.getPic()).into(viewHolder.food_pic);
        }

        viewHolder.food_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FoodInfoActivity.class);
                intent.putExtra("food_id",food.getFood_id().toString()+"");

                intent.putExtra("foodname",food.getFoodname().toString()+"");
                intent.putExtra("phonenum",phonenum);
                context.startActivity(intent);
            }
        });
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView food_pic;
        TextView food_foodname,food_price,food_intro;
        ConstraintLayout food_container;
        public ViewHolder(View itemView) {
            super(itemView);
            food_foodname = (TextView) itemView.findViewById(R.id.food_foodname);
            food_pic = (ImageView) itemView.findViewById(R.id.food_pic);
            food_price = (TextView) itemView.findViewById(R.id.food_price);
            food_intro = (TextView) itemView.findViewById(R.id.food_intro);
            food_container = (ConstraintLayout) itemView.findViewById(R.id.food_container);
        }
    }
}

