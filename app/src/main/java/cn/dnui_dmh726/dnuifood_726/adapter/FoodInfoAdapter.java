package cn.dnui_dmh726.dnuifood_726.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cn.dnui_dmh726.dnuifood_726.bean.CommentInfo;

public class FoodInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
private Context context;
private List<CommentInfo> list;
private LayoutInflater layoutInflater;

public FoodInfoAdapter(Context context, List<CommentInfo> list, int layoutResource) {
        this.context=context;
        this.list=list;
        layoutInflater = LayoutInflater.from(context);
        }

public void setList(List<CommentInfo> list) {
        this.list = list;
        }

@Override
public int getItemCount() {
        return list.size();
        }


@Override
public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = layoutInflater.from(viewGroup.getContext()).inflate(R.layout.shop_item3, viewGroup, false);
        return new FoodInfoAdapter.ViewHolder(v);
        }

@Override
public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
final CommentInfo food = (CommentInfo) list.get(position);
        FoodInfoAdapter.ViewHolder viewHolder = (FoodInfoAdapter.ViewHolder) holder;

        viewHolder.username.setText(food.getUsername());
        viewHolder.food_time.setText(food.getComment_time());
        viewHolder.food_comments.setText(food.getContent());

        }


public static class ViewHolder extends RecyclerView.ViewHolder{

    TextView username,food_time,food_comments;
    LinearLayout food;
    public ViewHolder(View itemView) {
        super(itemView);
        username = (TextView) itemView.findViewById(R.id.res_name);
        food_time = (TextView) itemView.findViewById(R.id.res_time);
        food_comments = (TextView) itemView.findViewById(R.id.res_jianjie);
        food=itemView.findViewWithTag(R.id.one);

    }
}
}