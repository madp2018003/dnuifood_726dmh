package cn.dnui_dmh726.dnuifood_726.model;



import java.util.List;

import cn.dnui_dmh726.dnuifood_726.bean.GetCollected;
import cn.dnui_dmh726.dnuifood_726.bean.ResultBean;
import cn.dnui_dmh726.dnuifood_726.bean.UserBean;
import cn.dnui_dmh726.dnuifood_726.iface.Useriface;
import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;
import cn.dnui_dmh726.dnuifood_726.service.UserService;
import retrofit2.Call;

/**
 * Created by X on 2018/10/15.
 */

public class UserModel extends BaseModel implements Useriface {
    private UserService service;

    public UserModel(){
        service=retrofit.create(UserService.class);
    }

    @Override
    public void login(String username, String userpass, final onRetrofitListener listener) {
        service=retrofit.create(UserService.class);

        Call<UserBean> call=service.doLogin(username,userpass);
        bindCallback(call,listener,1);
//         call.enqueue(new Callback<UserBean>() {
//
//             @Override
//             public void onResponse(Call<UserBean> call, Response<UserBean> response) {
//                 if(response.body()!=null)
//                     listener.onSuccess(response.body(),);
//
//                   else
//                   listener.onFailure("解析错误!");
//             }
//
//             @Override
//             public void onFailure(Call<UserBean> call, Throwable t) {
//
//             }
//
//         });


    }

    @Override
    public void register(String username, String userpass, String mobilenum, String address, String comment, final onRetrofitListener listener) {
        service = retrofit.create(UserService.class);

        Call<ResultBean> call = service.doRegist(username, userpass, address, mobilenum, comment);
        bindCallback(call, listener, 1);

    }

    @Override
    public void getAllUserCollection(String user_id, String flag, onRetrofitListener listener) {
        service = retrofit.create(UserService.class);

        Call<List<GetCollected>> call = service.getAllUserCollection(user_id, flag);
        bindCallback(call, listener, 1);
    }
    @Override
    public void userCollectShop(String user_id, String shop_id, onRetrofitListener listener) {

        Call call = service.userCollectShop(user_id, shop_id);
        bindCallback(call, listener, 2);

    }



    public void userCollectFood(String user_id, String food_id, onRetrofitListener listener) {

        Call call=service.userCollectFood(user_id,food_id);
        bindCallback(call,listener,3);
    }
    @Override
    public void getUserById(String user_id, onRetrofitListener listener) {
        Call call = service.getUserById(user_id);
        bindCallback(call,listener,4);
    }

    @Override
    public void updateUserById(String user_id, String username, String userpass, String mobilenum, String address, onRetrofitListener listener) {
        Call call =service.updateUserById(user_id,username,userpass,mobilenum,address);
        bindCallback(call,listener,5);
    }




}