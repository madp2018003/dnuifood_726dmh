package cn.dnui_dmh726.dnuifood_726.model;

import cn.dnui_dmh726.dnuifood_726.iface.Shopiface;
import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;
import cn.dnui_dmh726.dnuifood_726.service.ShopService;
import retrofit2.Call;

/**
 * Created by X on 2018/10/29.
 */

public class ShopModel extends BaseModel implements Shopiface {

    private ShopService shopService;
    private String flag;

    public ShopModel(){
        shopService=retrofit.create(ShopService.class);

    }
    public  void  getAllShops(onRetrofitListener listener){
        Call call=shopService.getAllshops();
        bindCallback(call,listener,1);
    }

    @Override
    public void getFoodByShop(String shop_id, onRetrofitListener listener) {
        Call call=shopService.getFoodByShop(shop_id);
        bindCallback(call,listener,2);
    }


    @Override
    public void isCollected(String user_id, String shop_food_id, String flag, onRetrofitListener listener) {
        Call call=shopService.isCollected(user_id,shop_food_id,flag);
        bindCallback(call,listener,3);
    }

    @Override
    public void userCollectShop(String user_id, String shop_id, onRetrofitListener listener) {
        Call call=shopService.userCollectShop(user_id,shop_id);
        bindCallback(call,listener,4);
    }
}