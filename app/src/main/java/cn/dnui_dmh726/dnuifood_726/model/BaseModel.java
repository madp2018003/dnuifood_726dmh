package cn.dnui_dmh726.dnuifood_726.model;


import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by X on 2018/10/15.
 */

public class BaseModel {
    public Retrofit retrofit;
    private static String BaseUrl = "http://172.24.10.175:8080/foodService/";

    public BaseModel() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public BaseModel(String type) {
        if (type != "")
            retrofit = new Retrofit.Builder()
                    .baseUrl(BaseUrl)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();

    }


    public <T> void bindCallback(Call<T> call, final onRetrofitListener<T> listener, final int flag) {
        Callback<T> callback = new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                listener.onSuccess(response.body(), flag);
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                listener.onFailure(t.toString());
            }
        };
        call.enqueue(callback);
    }


}