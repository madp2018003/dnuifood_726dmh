package cn.dnui_dmh726.dnuifood_726.model;


import java.util.List;

import cn.dnui_dmh726.dnuifood_726.bean.CommentInfo;
import cn.dnui_dmh726.dnuifood_726.bean.SearchBean;
import cn.dnui_dmh726.dnuifood_726.iface.FoodIface;
import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;
import cn.dnui_dmh726.dnuifood_726.service.FoodService;
import retrofit2.Call;

/**
 * Created by X on 2018/11/19.
 */

public class FoodModel extends BaseModel implements FoodIface {
    private FoodService foodService;


    public FoodModel(){
        foodService=retrofit.create(FoodService.class);

    }
    public void isCollected(String user_id, String shop_food_id, String flag, onRetrofitListener listener) {
        Call call=foodService.isCollected(user_id,shop_food_id,flag);
        bindCallback(call,listener,3);
    }


    public void userCollectFood(String user_id, String food_id, onRetrofitListener listener) {
        Call call=foodService.userCollectFood(user_id,food_id);
        bindCallback(call,listener,4);
    }
    public void getFoodById(String food_id, onRetrofitListener listener) {
        Call call = foodService.getFoodById(food_id);
        bindCallback(call,listener, 1); }



    public void getAllCommentsByFood(String food_id, onRetrofitListener listener) {

        Call <List<CommentInfo>>call = foodService.getAllCommentsByFood(food_id);
        bindCallback(call,listener,2);
    }

    @Override
    public void getFoodBySearch(String search, onRetrofitListener listener) {
        Call <List<SearchBean>>call=foodService.getFoodBySearch(search);
        bindCallback(call,listener,5);
    }

    @Override
    public void getAllOrdersByUser(String user_id, onRetrofitListener listener) {
        Call call = foodService.getAllOrdersByUser(user_id);
        bindCallback(call,listener,6);
    }

    @Override
    public void getAllCommentsByUser(String user_id, onRetrofitListener listener) {
        Call call = foodService.getAllCommentsByUser(user_id);
        bindCallback(call,listener,7);
    }

    @Override
    public void insertComment(String item_id, String content, onRetrofitListener listener) {
        Call call = foodService.insertComment(item_id,content);
        bindCallback(call,listener,8);
    }

    @Override
    public void updateComment(String item_id, String content, onRetrofitListener listener) {
        Call call = foodService.updateComment(item_id,content);
        bindCallback(call,listener,9);
    }

    @Override
    public void deleteComment(String item_id, onRetrofitListener listener) {
        Call call = foodService.deleteComment(item_id);
        bindCallback(call,listener,10);
    }



}