package cn.dnui_dmh726.dnuifood_726.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import cn.dnui_dmh726.dnuifood_726.R;
import cn.dnui_dmh726.dnuifood_726.adapter.CollectAdapter;
import cn.dnui_dmh726.dnuifood_726.bean.GetCollected;
import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;
import cn.dnui_dmh726.dnuifood_726.model.UserModel;

/**
 * Created by X on 2018/11/27.
 */

public class CollectShopFragment extends BaseFragment implements onRetrofitListener<List<GetCollected>> {
    private RecyclerView collect_rv;
    private CollectAdapter adapter;
    private UserModel userModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setLayout_file(R.layout.collectshopfragment);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initViews() {
        collect_rv = (RecyclerView)view.findViewById(R.id.rv);
        collect_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        collect_rv.setHasFixedSize(true);
    }

    @Override
    void initEvents() {

    }


    @Override

    void initData() {
        userModel=new UserModel();
        userModel.getAllUserCollection(getUser_id(),"0",this);

    }


    @Override
    public void onSuccess(List<GetCollected> object, int flag) {
        adapter=new CollectAdapter(context, object,R.layout.shop_item4);
        collect_rv.setAdapter(adapter);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(context, "失败："+msg, Toast.LENGTH_SHORT).show();
    }
}