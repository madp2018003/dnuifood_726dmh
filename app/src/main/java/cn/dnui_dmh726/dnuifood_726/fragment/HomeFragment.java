package cn.dnui_dmh726.dnuifood_726.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import java.util.List;

import cn.dnui_dmh726.dnuifood_726.R;
import cn.dnui_dmh726.dnuifood_726.adapter.ShopAdapter;
import cn.dnui_dmh726.dnuifood_726.bean.ShopBean;
import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;
import cn.dnui_dmh726.dnuifood_726.model.ShopModel;

/**
 * Created by X on 2018/11/1.
 */



public class HomeFragment extends BaseFragment implements onRetrofitListener<List<ShopBean>> {
    private RecyclerView recyclerView;
    private ShopAdapter adapter;
    private List<ShopBean> mDataList;
    private LinearLayoutManager   layoutManager;
    private int page=1; // 代表页数，并初始化为1，代表第1页。
    private int lastVisibleItemPosition;//最后一条可见条目的位置

    @Override
    void initViews() {

    }

    @Override
    void initEvents() {

    }

    @Override
    void initData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.liebiao,container,false);

    }
    @Override//生命周期方法，View创建完成
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView(view);
        ShopModel model=new ShopModel();
        model.getAllShops(this);
//        ShopAdapter.setOnItemClickListener(new ShopAdapter.OnRecyclerViewItemClickListener(){
//            @Override
//            public void onItemClick(View view , Object data){
//                Shop shop=(Shop)data;
//                Toast.makeText(MainActivity.this, shop.getShopname(), Toast.LENGTH_LONG).show(); }
//        });

    }
    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.rv01);
        // 设置布局
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //每个item如果是确定高度，设置此项提高性能// 设置动画效果
        recyclerView.setHasFixedSize(true);
        //实例化适配器

        //以下为新增内容
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == list.size()) {
//                    page += 1;
//                    //再次实例化ArticleModel，调用方法获取网络数据，请求新一页数据
//                    ShopModel shopModel = new ShopModel();
//                    shopModel.getAllShops();
//                }
//            }
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();//滚动结束后将赋值为可见条目中最后一条位置
//            }
//        });


    }

    @Override
    public void onSuccess(List<ShopBean> shops, int flag) {
        adapter = new ShopAdapter(getActivity(), shops);
        recyclerView.setAdapter(adapter);
        //设置适配器
        if (page == 1) {
            mDataList = shops;
        } else {
            mDataList.removeAll(shops);
            mDataList.addAll(shops);
        }
        adapter.setList(shops);



    }
    @Override
    public void onFailure(String msg) {
        Toast.makeText(context, "失败："+msg, Toast.LENGTH_SHORT).show();

    }




}
