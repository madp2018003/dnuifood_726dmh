package cn.dnui_dmh726.dnuifood_726.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;


import java.util.List;

import cn.dnui_dmh726.dnuifood_726.R;
import cn.dnui_dmh726.dnuifood_726.controller.SearchResultActivity;
import cn.dnui_dmh726.dnuifood_726.db.RecordDBOperation;
import cn.dnui_dmh726.dnuifood_726.db.RecordSQLiteOpenHelper;

/**
 * Created by X on 2018/12/4.
 */

public class SearchFragment extends BaseFragment  {
    private TextView tv_clear,tv_tip;
    private EditText et_search;
    private Button btn_ok;

    private ListView listView;
    // private Context context;
    RecordDBOperation recordDB;
    RecordSQLiteOpenHelper helper;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.searchfragment, container, false);
        helper=new RecordSQLiteOpenHelper(context);
        recordDB=new RecordDBOperation(helper);
        initViews();
        initEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        helper=new RecordSQLiteOpenHelper(context);
        recordDB=new RecordDBOperation(helper);
    }

    @Override
    public void onStop() {
        super.onStop();
        recordDB.closedb();
    }
    @Override
    void initViews() {
        tv_clear=view.findViewById(R.id.tv_clear);
        tv_tip=view.findViewById(R.id.tv_tip);
        et_search=view.findViewById(R.id.et_search);
        btn_ok=view.findViewById(R.id.btn_ok);
        listView=view.findViewById(R.id.listView);

    }

    @Override
    void initEvents() {
        // 清空搜索历史
        tv_clear.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {


                recordDB.deleteData();
                showData(""); }

        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordDB.insertData(et_search.getText().toString().trim());
                showDetailInfo(et_search.getText().toString().trim()); }
        });

// 搜索框的键盘搜索键点击回调
        et_search.setOnKeyListener(new View.OnKeyListener() {// 输入完后按键盘上的搜索键
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() ==
                        KeyEvent.ACTION_DOWN) {// 修改回车键功能 // 先隐藏键盘
                    ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
                            getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

// 按完搜索键后将当前查询的关键字保存起来,如果该关键字已经存在就不执行保存

                    boolean hasData = recordDB.hasData(et_search.getText().toString().trim());
                    if (!hasData) {
                        recordDB.insertData(et_search.getText().toString().trim());
                        showData(""); }
                    //根据输入的内容模糊查询商品，并跳转到另一个界面，由你自己去实现
                    showDetailInfo(et_search.getText().toString().trim()); }
                return false; }
        });
        // 搜索框的文本变化实时监听
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                btn_ok.setVisibility(View.VISIBLE);
                if (s.toString().trim().length() == 0) {
                    tv_tip.setText("搜索历史");
                } else { tv_tip.setText("搜索结果");
                }
                String tempName = et_search.getText().toString();
                showData(tempName);
            } });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() { @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            String name = textView.getText().toString();

            et_search.setText(name);
            showDetailInfo(name); }
        }); }

    @Override
    void initData() {

    }


    void showData(String tempName) {

        List list = recordDB.queryData(tempName);
        // 创建 adapter 适配器对象
        SimpleAdapter adapter = new SimpleAdapter(getActivity(), list, android.R.layout.simple_list_item_1, new
                String[]{"name"}, new int[]{android.R.id.text1});
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    void showDetailInfo(String name) {

        Intent intent = new Intent(context, SearchResultActivity.class);
        intent.putExtra("search",et_search.getText().toString().trim());
        startActivity(intent);
    }




}