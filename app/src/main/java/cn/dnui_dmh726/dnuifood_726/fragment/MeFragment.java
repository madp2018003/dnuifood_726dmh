package cn.dnui_dmh726.dnuifood_726.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.dnui_dmh726.dnuifood_726.R;
import cn.dnui_dmh726.dnuifood_726.bean.UserBean;
import cn.dnui_dmh726.dnuifood_726.controller.AlterUserInfoActivity;
import cn.dnui_dmh726.dnuifood_726.controller.MyCommentActivity;
import cn.dnui_dmh726.dnuifood_726.controller.MyOrderActivity;
import cn.dnui_dmh726.dnuifood_726.controller.PhotoActivity;
import cn.dnui_dmh726.dnuifood_726.controller.SettingActivity;
import cn.dnui_dmh726.dnuifood_726.listener.onRetrofitListener;
import cn.dnui_dmh726.dnuifood_726.model.UserModel;


/**
 * Created by X on 2018/11/1.
 */

public class MeFragment extends BaseFragment implements onRetrofitListener<UserBean> {
    private ImageView my_setting;
    private ImageView user_photo,go;
    private TextView my_username, my_telephone;
    private LinearLayout my_userinfo, my_order, my_comment;
    private UserBean userBean;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setLayout_file(R.layout.mefragment);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    @Override
    void initViews() {
        my_username = (TextView) view.findViewById(R.id.my_username);
        my_telephone = (TextView) view.findViewById(R.id.my_telephone);
        my_setting = (ImageView) view.findViewById(R.id.my_setting);
        my_userinfo = (LinearLayout) view.findViewById(R.id.my_userinfo);
        my_order = (LinearLayout) view.findViewById(R.id.my_order);
        my_comment = (LinearLayout) view.findViewById(R.id.my_comment);
        go=view.findViewById(R.id.imageView5);
        user_photo=(ImageView)view.findViewById(R.id.user_photo);
    }

    @Override
    void initEvents() {

        my_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SettingActivity.class);
                startActivity(intent);
            }
        });
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AlterUserInfoActivity.class);
                intent.putExtra("username",userBean.getUsername());
                intent.putExtra("telephone",userBean.getMobilenum());
                intent.putExtra("address",userBean.getAddress());
                context.startActivity(intent);
            }
        });
        user_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PhotoActivity.class);
                startActivity(intent);
            }
        });
        my_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrderActivity.class);
                startActivity(intent);
            }
        });
        my_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyCommentActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    void initData() {
        UserModel userModel = new UserModel();
        userModel.getUserById(getUser_id(),this);
    }



    @Override
    public void onSuccess(UserBean userBean, int flag) {
        this.userBean=userBean;
        my_username.setText(userBean.getUsername());
        my_telephone.setText(userBean.getMobilenum());
    }

    @Override
    public void onFailure(String msg) {

    }
    @Override
    public void onResume() {
        super.onResume();
        initData(); }
}