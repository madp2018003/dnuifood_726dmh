package cn.dnui_dmh726.dnuifood_726.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.dnui_dmh726.dnuifood_726.R;


public abstract class BaseFragment extends Fragment {
    private String userid;
    protected int layout_file=0;
    private SharedPreferences sp;
    protected FragmentManager childManager;
    private FragmentTransaction transaction;
    protected Context context;
    protected View view = null;
    //与sharedpreferences保存的关键字一致
    private final String KEY_USERNAME = "name";
    //与sharedpreferences保存的关键字一致
    private final String KEY_USERID = "user_id";
    private final String KEY_USERPASS = "pass";


    private final String FILE = "login";
    //与sharedpreferences的文件名一致
    private final int MODE = Context.MODE_PRIVATE;

    private SharedPreferences sharedPreferences;

    abstract void initViews();
    abstract void initEvents();

    abstract void initData();

    void setLayout_file(int layout_file) {
        this.layout_file=layout_file; }
    int getLayout_file() {
        return this.layout_file; }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        view = inflater.inflate(getLayout_file(), container, false);
        initViews();
        initEvents();
        initData();
        return view;
    }

    public String getUser_id() {
        sp = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        return sp.getString("user_id", "");
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        childManager = getChildFragmentManager();
    }
    protected void addFragment(Fragment fragment) {
        transaction = childManager.beginTransaction();
        transaction.add(R.id.collectfragment, fragment);
        transaction.commit();
    }




    @Override  //Fragment生命周期方法
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        sharedPreferences = context.getSharedPreferences(FILE, MODE);
    }

    protected void replaceFragment(Fragment fragment) {
        transaction = childManager.beginTransaction();
        transaction.replace(R.id.collectfragment, fragment);
        transaction.commit();
    }
    protected void removeFragment(Fragment fragment) {
        transaction = childManager.beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }

    //返回username
    protected String getUserName(){
        return sharedPreferences.getString(KEY_USERNAME, "");

    }
    protected String getUserPass(){
        return sharedPreferences.getString(KEY_USERPASS, "");

    }
    protected String getUserid() {
        return sharedPreferences.getString(KEY_USERID, userid);


    }
}
