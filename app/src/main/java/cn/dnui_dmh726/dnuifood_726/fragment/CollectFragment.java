package cn.dnui_dmh726.dnuifood_726.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cn.dnui_dmh726.dnuifood_726.R;

/**
 * Created by X on 2018/11/1.
 */

public class CollectFragment extends Fragment {
    private TextView collect_shop,collect_food;
    private CollectShopFragment collectShopFragment;
    private CollectFoodFragment collectFoodFragment;
    private View view = null;
    protected FragmentManager childManager;
    private FragmentTransaction transaction;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) { view = inflater.inflate(R.layout.fragment_container, container, false);
        initViews();
        initEvents();
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        if (collectShopFragment == null) {
            collectShopFragment =  new CollectShopFragment();
            addFragment(collectShopFragment);
        } else {
            replaceFragment(collectShopFragment);
        } }

    private void initViews(){
        childManager = getChildFragmentManager();
        collect_shop = (TextView)view.findViewById(R.id.collect_shop);
        collect_food = (TextView)view.findViewById(R.id.collect_food);
        collect_shop.setSelected(true);
        collect_shop.setTextColor(getResources().getColor(R.color.cardview_dark_background));
        collect_food.setTextColor(getResources().getColor(R.color.colorPrimary));
    }
    private void initEvents(){
        collect_shop.setOnClickListener(new View.OnClickListener() { @Override
        public void onClick(View v) {
            if (collectShopFragment == null) {
                collectShopFragment =  new CollectShopFragment();
            }
            replaceFragment(collectShopFragment);
            collect_shop.setSelected(true);
            collect_food.setSelected(false);
            collect_shop.setTextColor(getResources().getColor(R.color.cardview_dark_background));
            collect_food.setTextColor(getResources().getColor(R.color.colorPrimary));
        } });
        collect_food.setOnClickListener(new View.OnClickListener() { @Override
        public void onClick(View v) {
            if (collectFoodFragment == null) {
                collectFoodFragment = new  CollectFoodFragment();
            }
            replaceFragment(collectFoodFragment);
            collect_shop.setSelected(false);
            collect_food.setSelected(true);
            collect_food.setTextColor(getResources().getColor(R.color.cardview_dark_background));
            collect_shop.setTextColor(getResources().getColor(R.color.colorPrimary));
        } });
    }
    protected void addFragment(Fragment fragment) {
        transaction = childManager.beginTransaction();
        transaction.add(R.id.fragment_container, fragment);
        transaction.commit();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context); }
    protected void replaceFragment(Fragment fragment) {
        transaction = childManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }
}

