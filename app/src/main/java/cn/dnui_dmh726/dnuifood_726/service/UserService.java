package cn.dnui_dmh726.dnuifood_726.service;



import java.util.List;

import cn.dnui_dmh726.dnuifood_726.bean.GetCollected;
import cn.dnui_dmh726.dnuifood_726.bean.ResultBean;
import cn.dnui_dmh726.dnuifood_726.bean.UserBean;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by X on 2018/10/15.
 */

public interface UserService {
    @GET("userLogin.do")
    Call<UserBean>doLogin(
            @Query("username") String username,
            @Query("userpass") String password);

    @GET("userRegister.do")
    Call<ResultBean>doRegist(
            @Query("username") String username,
            @Query("userpass") String password,
            @Query("mobilenum") String phonenumber,
            @Query("addredd") String addredd,
            @Query("comment") String comment
    );

    @GET("getAllUserCollection.do")
    Call<List<GetCollected>>getAllUserCollection(
            @Query("user_id") String user_id,
            @Query("flag") String flag
    );
    @GET("userCollectShop.do")
    Call<ResultBean> userCollectShop(
            @Query("user_id") String user_id,
            @Query("food_id") String shop_id
    );
    @GET("userCollectFood.do")
    Call<ResultBean>userCollectFood(
            @Query("user_id") String user_id,
            @Query("food_id") String food_id
    );
    @GET("getUserById.do")
    Call<UserBean>getUserById(
            @Query("user_id") String user_id
    );
    @GET("updateUserById.do")
    Call<ResultBean> updateUserById(

            @Query("user_id") String user_id,
            @Query("username") String username,
            @Query("userpass") String userpass,
            @Query("mobilenum") String mobilenum,
            @Query("address") String address
    );
}