package cn.dnui_dmh726.dnuifood_726.service;


import java.util.List;

import cn.dnui_dmh726.dnuifood_726.bean.CollectStatus;
import cn.dnui_dmh726.dnuifood_726.bean.CommentInfo;
import cn.dnui_dmh726.dnuifood_726.bean.FoodInfo;
import cn.dnui_dmh726.dnuifood_726.bean.ResultBean;
import cn.dnui_dmh726.dnuifood_726.bean.SearchBean;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by X on 2018/11/19.
 */
    public interface FoodService {
        @GET("getFoodById.do")
        Call<FoodInfo> getFoodById(

                @Query("food_id") String food_id

        );

        @GET("getAllCommentsByFood.do")
        Call<List<CommentInfo>> getAllCommentsByFood(
                @Query("food_id") String _id
        );

        @GET("isCollected.do")
        Call<CollectStatus> isCollected(
                @Query("user_id") String user_id,
                @Query("shop_food_id") String shop_food_id,
                @Query("flag") String flag

        );

        @GET("userCollectFood.do")
        Call<ResultBean> userCollectFood(
                @Query("user_id") String user_id,
                @Query("food_id") String food_id
        );

        @GET("getFoodBySearch.do")
        Call<List<SearchBean>> getFoodBySearch(
                @Query("search") String search
        );

        @GET("getAllOrdersByUser.do")
        Call<List<CommentInfo>> getAllOrdersByUser(

                @Query("user_id") String user_id);

        @GET("getAllCommentsByUser.do")
        Call<List<CommentInfo>> getAllCommentsByUser(

                @Query("user_id") String user_id);

        @GET("insertComment.do")
        Call<ResultBean> insertComment(

                @Query("item_id") String item_id,
                @Query("content") String content

        );

        @GET("updateComment.do")
        Call<ResultBean> updateComment(

                @Query("item_id") String item_id,
                @Query("content") String content
        );

        @GET("deleteComment.do")
        Call<ResultBean> deleteComment(

                @Query("item_id") String item_id

        );
    }
