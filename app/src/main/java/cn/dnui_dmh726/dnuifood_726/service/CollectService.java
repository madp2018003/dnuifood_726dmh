package cn.dnui_dmh726.dnuifood_726.service;


import cn.dnui_dmh726.dnuifood_726.bean.CollectStatus;
import cn.dnui_dmh726.dnuifood_726.bean.ResultBean;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by X on 2018/11/19.
 */

public interface CollectService {
    @GET("isCollected.do")
    Call<CollectStatus> isCollected(@Query("user_id") String user_id,
                                    @Query("shop_food_id") String shop_food_id,
                                    @Query("flag") String flag);

    @GET("userCollectShop.do")
    Call<ResultBean> userCollectShop(@Query("user_id") String user_id,
                                     @Query("shop_id") String shop_id);



}
