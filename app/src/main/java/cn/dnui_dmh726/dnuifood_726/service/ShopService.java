package cn.dnui_dmh726.dnuifood_726.service;


import java.util.List;

import cn.dnui_dmh726.dnuifood_726.bean.CollectStatus;
import cn.dnui_dmh726.dnuifood_726.bean.FoodBean;
import cn.dnui_dmh726.dnuifood_726.bean.ResultBean;
import cn.dnui_dmh726.dnuifood_726.bean.ShopBean;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by X on 2018/10/29.
 */

public interface ShopService {
    @GET("getAllShops.do")
    Call<List<ShopBean>>getAllshops();

    @GET("getFoodByShop.do")
    Call<List<FoodBean>>getFoodByShop(
            @Query("shop_id") String shop_id
    );
    @GET("isCollected.do")
    Call<CollectStatus>isCollected(
            @Query("user_id") String user_id,
            @Query("shop_food_id") String shop_food_id,
            @Query("flag") String flag

    );
    @GET("userCollectShop.do")
    Call<ResultBean>userCollectShop(
            @Query("user_id") String user_id,
            @Query("shop_id") String shop_id
    );

}